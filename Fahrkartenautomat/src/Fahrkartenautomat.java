﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		do {
			zuZahlenderBetrag = farkartenBestellungErfassen();
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
			System.out.println("Neuer Kaufvorgang");
		} while (true);
	}

	public static double farkartenBestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);

		// Durch die Implementierung von Arrays wird der Code deutlich kürzer, denn
		// viele einzelne Ausgaben werden durch das Durchlaufen der Arrays verkürzt.
		// Außerdem ist es durch diese Methode möglich Fahrkarten und Fahrkartenpreise
		// zu integrieren ohne den nachfolgenden Code zu verändern.
		// Diese Vorgehensweise ist komplizierter als die Vorgehensweise ohne Arrays,
		// aber nützlich für die Komprimierung, Erweiterung und Wartbarkeit des
		// Quellcodes.

		String[] fahrkarten = { "(1) Einzelfahrschein AB [Preis: 2,90 €]", "(2) Einzelfahrschein BC [Preis: 3,30 €]",
				"(3) Einzelfahrschein ABC [Preis: 3,60 €]", "(4) Kurzstrecke [Preis: 1,90 €]",
				"(5) Tageskarte Berlin AB [Preis: 8,60 €]", "(6) Tageskarte Berlin BC [Preis: 9,00 €]",
				"(7) Tageskarte Berlin ABC [Preis: 9,60 €]", "(8) Kleingruppen Tageskarte AB [Preis: 23,50 €]",
				"(9) Kleingruppen Tageskarte BC [Preis: 24,30 €]",
				"(10) Kleingruppen Tageskarte ABC [Preis: 24,90 €]" };

		for (int i = 0; i < fahrkarten.length; i++) {
			System.out.println(fahrkarten[i]);
		}

		System.out.println("");
		System.out.println("Bitte geben Sie die Nummer der von Ihnen gewünschten Fahrkarte ein!");

		int benutzerwahl = tastatur.nextInt();
		benutzerwahl = benutzerwahl - 1;
		double[] preisFuerEinTicket = { 2.9, 3.3, 3.6, 1.9, 8.6, 9.0, 9.6, 23.5, 24.3, 24.9 };
		System.out.println("");

		if (benutzerwahl >= 0 && benutzerwahl < preisFuerEinTicket.length) {
			System.out.printf("Preis der Fahrkarte: %.2f €!" ,preisFuerEinTicket[benutzerwahl]);
		}

		else {
			System.out.println("Ungügltige Eingabe. Einzelfahrschein AB wurde ausgewählt.");
			benutzerwahl = 0;
		}

		System.out.println("Anzahl der Tickets: ");
		int anzahlTicket = tastatur.nextInt();

		if (anzahlTicket > 10 || anzahlTicket < 1) {
			anzahlTicket = 1;

			System.out.println("Ungültige Anzahl an Tickets eingegeben! 1 Fahrkarte wurde ausgewählt!");
		}

		double zuZahlenderBetrag;

		zuZahlenderBetrag = preisFuerEinTicket[benutzerwahl] * anzahlTicket;
		return zuZahlenderBetrag;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double ausstehendeBetrage = zuZahlenderBetrag;
			ausstehendeBetrage = ausstehendeBetrage - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f €\n", ausstehendeBetrage);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		Scanner tastatur = new Scanner(System.in);

		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		Scanner tastatur = new Scanner(System.in);

		double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rückgabebetrag > 0.0) {
			System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückgabebetrag -= 2.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückgabebetrag -= 1.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückgabebetrag -= 0.5;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückgabebetrag -= 0.2;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückgabebetrag -= 0.1;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückgabebetrag -= 0.05;

			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");

	}

}